"use client";

import { useState, useEffect } from "react";
import {
  PaymentElement,
  useStripe,
  useElements,
} from "@stripe/react-stripe-js";
import formatPrice from "@/utils/PriceFormat";

export default function CheckoutForm({
  clientSecret,
}: {
  clientSecret: string;
}) {
  const [isLoading, setIsLoading] = useState(false);

  const stripe = useStripe();
  const elements = useElements();

  return (
    <form id="payment-form">
      <PaymentElement id="payment-Element" options={{ layout: "tabs" }} />
      <h1>Total: {}</h1>
      <button id="submit" disabled={isLoading || !stripe || !elements}></button>
    </form>
  );
}
